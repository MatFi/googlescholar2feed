import logging
import sys
logging.basicConfig(stream=sys.stderr)
path = '/var/www/liebeleu.de/googlescholar2feed'
if path not in sys.path:
   sys.path.insert(0, path)
#activate_this = '/var/www/liebeleu.de/googlescholar2feed/env/bin/activate'
#with open(activate_this) as file_:
#    exec(file_.read(), dict(__file__=activate_this))
from gs2feed.app import app as application
