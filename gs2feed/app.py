from flask import Flask, request, send_file
from datetime import timedelta, datetime
from gs2feed.getScholar import ScholarQuery

app = Flask("__name__")

@app.before_first_request
def load_global_data():

    global feeds
    feeds = dict()


@app.route('/res/<id>')
def get_resource(id):
    if id == 'logo.png':
       return send_file('res/logo.png', mimetype='image/png')
    else:
        filename = 'error.gif'
    return ""


@app.route('/scholar')
def show_post():
        """
        Returns the rss feed . Therby it checks if the feed was already
        aquired already, and if so it delivers the cached one.

        How ever we usually have multiple calls her (favicon?)
        """
        req = request.full_path

        if req not in feeds or datetime.now()-feeds[req][1] > timedelta(minutes=10):
            try:
                ret = ScholarQuery(req).make_rss.rss_str(pretty=True)
                feeds[req] = [ret, datetime.now()]
            except IndexError as exc:

                ret = str(exc)
                pass
        else:
            ret = feeds[req][0]
        return ret

if __name__ == "__main__":
         app.run()

