from typing import List, Any

import requests
from bs4 import BeautifulSoup
import re
"""Der Plan ist mittels den daten von google scholar zu erkennen 
welcher container der webseite TITEL und ABSTRACT enthält, dieser
 container ist dann wohl der artikel"""

class FullAbstract:

    def __init__(self, **kwargs):
        """

        :param kwargs:
        """
        self.url= kwargs.get('url', None)
        keys = kwargs.get('keys', None)

        self.page = BeautifulSoup(self.__getpage(self.url), 'lxml')



    def __getpage(self, url):
        """
        Gets the raw page of google scholar
        We have to be aware of googles bot detect so we provide a nice header to the request
        """

        # self.url = 'https://google.de/search/?q=' + quarystr
        headers = {

            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'TE': 'Trailers',
        }

        content = requests.get(self.url, headers=headers).text
        return content

    def __get_articlebykey(self, keys):
        """
        Extracts the smallest part of the website which contains all the given keys
        Only 2 keys implemented so far
        :param bspage: Beautiful soap webpage description
        :param keys: two keyy strings
        :return: Beautifulsoap with the part of the website containing both keys
        """
        a_str = self.page.body.find(string=re.compile(keys[0]))
        a_par=[]
        b_str = self.page.body.find(string=re.compile(keys[1]))
        b_par=[]

        try:
            for i in a_str.parents :
                a_par.append(i)
            a_par=list(reversed(a_par))

            for i in b_str.parents:
                b_par.append(i)
            b_par=list(reversed(b_par))

            for i in range(0,len(a_par)-1):
                if a_par[i] != b_par[i]:
                    return a_par[i-1]
            return
        except:

            return


    def get_fulltext(self,keys):
        try:
            return self.__get_articlebykey(keys).text
        except Exception as e:
            print(e)
            return ""


    def get_abstract(self,abstract):
        try:

            return self.page.body.find(string=re.compile(abstract)).parent.text
        except Exception as e:
            return str(e)
        pass



# url="https://www.sciencedirect.com/science/article/pii/S0959437X99000313"
# title ="Interspersed repeats and other mementos of transposable elements in mammalian genomes"
# abstract="ultimately derived from transposable"
url="https://www.nature.com/articles/srep00591?page=4"
title="Lead Iodide Perovskite Sensitized All-Solid-State Submicron Thin Film Mesoscopic Solar Cell with Efficiency Exceeding 9"
abstract="We report on solid-state mesoscopic heterojunction solar cells employing nanoparticles (NPs) of methyl ammonium lead iodide"
t=FullAbstract(url=url)
print(t.get_abstract(abstract))

