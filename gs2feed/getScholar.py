
import requests
from bs4 import BeautifulSoup
from urllib.parse import urlparse, parse_qsl, urlencode
from feedgen.feed import FeedGenerator

from datetime import timedelta, datetime
import pytz



class ScholarQuery:


    def __init__(self, query):

        """
        When initialising we directly aquire the page. Before we check if there is a query at all
        :param query: the query string
        """

        self.authorName =  'Mathias Fischer'
        self.authorMail = 'mathias@liebeleu.de'
        self.hosturl = 'http://127.0.0.1:5000' # 'gs2feed.liebeleu.de'
        self.url = 'http://scholar.google.de/scholar?'
        self.qurl = self.url+query
        self.query = urlparse(self.qurl).query
        self.urlargs = dict(parse_qsl(self.query))

        """Parser only talks english"""
        self.urlargs['hl'] = 'en'

        """Force chronologic feed"""
        self.urlargs['scisbd'] = 1

        self.qurl =  self.url+ urlencode(self.urlargs)
        if self.query != '':
            page = self.__getpage(self.qurl)
        else:
            return

        self.page = BeautifulSoup(page, 'lxml')

        self.results = self.__parsepublicationlist(self.page)

        self.items = len(self.results)

    @staticmethod
    def __getpage(url):
        """
        Gets the raw page of google scholar
        We have to be aware of googles bot detect so we provide a nice header to the request
        """

        # self.url = 'https://google.de/search/?q=' + quarystr
        headers = {
            'Host': 'scholar.google.com',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:63.0) Gecko/20100101 Firefox/63.0',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'de,en-US;q=0.7,en;q=0.3',
            'Accept-Encoding': 'gzip, deflate, br',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
            'TE': 'Trailers',
            }

        content = requests.get(url, headers=headers).text
        return content

    @property
    def make_rss(self):

        fg = FeedGenerator()
        fg.id(self.qurl)
        fg.title("gs2feed: " + self.urlargs['q'])
        fg.author({'name': self.authorName, 'email': self.authorMail})
        fg.link(href=self.hosturl)
        fg.subtitle('when the alert comes too late')
        fg.language('en')
        fg.lastBuildDate(datetime.now(tz=pytz.utc))
        fg.image(self.hosturl +"/res/logo.png", width='143', height='143')
        for i in reversed(self.results):
            fe = fg.add_entry()
            fe.id(i['url'])
            fe.title(i['title'])
            fe.link(href=i['url'])
            fe.pubDate(i['age'])
            # fe.content(i['abstract'])
            contenttext = '<b>' + i['title'] + '</b><br>' + \
                '<small>' + i['author'] + "-" + i['journal'] + '</small><br>' + \
                '<b>Abstract: </b>' + i['abstract'] + '<br>' + \
                '<a href =https://sci-hub.tw/' + i['url'] + '> Sci-Hub </a>'
            if i['opentexturl'] != "":
                contenttext += ' <td>&nbsp;</td>   <a href =' + i['opentexturl'] + '> Full Text from source</a>'
            fe.content(contenttext, type='CDATA')

            fe.ttl(10)          # this is the updatelimit set in the flaskapp
        return fg

    @staticmethod
    def __parsepublicationlist(page):
        results = []
        for entry in page.find_all("div", attrs={"class": "gs_r"}):

            try:
                metaline = entry.find_all('div', attrs={"class": "gs_a"})[0].get_text()
                metaline = metaline.split('-')

                """get the age of the entry"""
                age = entry.find('span', attrs={"class": "gs_age"}).get_text()
                age = [int(s) for s in age.split() if s.isdigit()]
                age = datetime.now(tz=pytz.utc) - timedelta(days=age[0])

                abstract = entry.find('div', attrs={"class", "gs_rs"})
                abstract.span.decompose()   # remove the gs_age element from the abstract
                abstract =abstract.text
                opentexturl = entry.find('div', attrs={"class": 'gs_or_ggsm'})

                if opentexturl is None:
                    opentexturl = ""
                else:
                    opentexturl = opentexturl.a['href']
            except Exception as e:
                print(e)
            else:
                # abstract=FullAbstract(url=entry.h3.a['href']).get_abstract(abstract[:-3])
                results.append(
                   {"title": entry.h3.a.text, "author": metaline[0], "journal": metaline[1], "abstract": abstract,
                    "url": entry.h3.a['href'], "age": age, 'opentexturl': opentexturl})
        return results

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < self.items:
            self.n += 1
            return self.results[self.n-1]
        else:
            raise StopIteration

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        pass
